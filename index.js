// ES6 Updates

// Exponent Operator - **
const firstNum = 8**2;
console.log("Using ** Operator");
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log("Using Math.pow()");
console.log(secondNum);

// Template literals
/*
	-Allow us to write strings without using the concatenation operator(+)
*/

let name = "John";

let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Message without template literals: " + message);

// Strings using template literal
// Uses backticks (``)
message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals: ${message}`);

// Multi line using template literals
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.
`
console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// Array destructuring
/*
	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ['John', 'Peters', 'Smith'];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// Array destructuring example
const [firstName, middleName, lastName] = fullName;
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

const nation = ['USA', 'California'];
const [country, state] = nation;
console.log(`Welcome to ${state}, ${country}`);

// Object Destructuring
const person = {
	givenName: "Jane",
	maidenName: "Doe",
	familyName: "Smith"
}
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring
const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again`);

function getFullName ({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// Arrow function
/*
	Syntax:
		const variableName = () => {
			console.log();
		}
*/

const hello = () => {
	console.log("Hello, world!");
}
hello();
console.log(hello());

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John", "D", "Smith");

const students = ['John', 'Jane', 'Judy'];

// Arrow functions with loops
// Pre-arrow function
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

console.log('Using arrow function');
// Arrow function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statement

function add(x, y){
	return x + y;
}
let total = add(1, 2);
console.log(total);
console.log(add(1,2));

// Arrow function
console.log('Using arrow function');
const addNew = (x, y) => x + y;
let totalNew = addNew(1,3);
console.log(totalNew);

// Default Function Argument Value
const greet = (name = 'User') => {
	return `Good Morning, ${name}`;
}
console.log(greet());
console.log(greet('Rahul'));